#!/usr/bin/env python3

"""
Copyright 2018 Fritz Alexander Francisco <fritz.a.francisco@gmail.com>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
import cv2
import argparse
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# input arguments
ap = argparse.ArgumentParser()
ap.add_argument('-i','--input',type=str,required=True, help="path to calibration file")
args = vars(ap.parse_args())
print(args)

data = np.load(args['input'])

R1, R2, P1, P2, Q, validPixROI1, validPixROI2  = cv2.stereoRectify(data['cameraMatrix1'], data['distCoeffs1'], data['cameraMatrix2'], data['distCoeffs2'],(3840, 2160),R=data['R'],T=data['T'], flags=cv2.CALIB_ZERO_DISPARITY,alpha=1)

fig = plt.figure()
fig.patch.set_facecolor('black')
ax = fig.add_subplot(111, projection='3d')

for i,idx in enumerate(data['objectPoints']):

 # Create 3D points through triangulation
    X = cv2.triangulatePoints(P1, P2, data['imagePoints1'][i], data['imagePoints2'][i])

    # Remember to divide out the 4th row. Make it homogeneous
    X /= X[3]
    X = X.T

    # # Recover the origin arrays from PX
    # x1 = np.dot(P1[:3],X)
    # x2 = np.dot(P2[:3],X)
    # # Again, put in homogeneous form before using them
    # x1 /= x1[2]
    # x2 /= x2[2]

    # plot with matplotlib
    Ys = X[:, 0]
    Zs = X[:, 1]
    Xs = X[:, 2]

    ax.scatter(Xs, Ys, Zs, marker='o')
ax.set_xlabel('Y')
ax.set_ylabel('Z')
ax.set_zlabel('X')
plt.title('3D point cloud')
plt.show()
