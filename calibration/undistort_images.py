#!/usr/bin/env python3

"""
Copyright 2018 Fritz Alexander Francisco <fritz.a.francisco@gmail.com>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import cv2
import numpy as np
import argparse
import os
from glob import glob

def parse_input():
    parser = argparse.ArgumentParser(description = 'Undistort images.')
    parser.add_argument('-d', '--image_directory',required=True, help = 'full path to image directory')
    parser.add_argument('-e', '--file_extension',required=True, help = 'file extension of images')
    parser.add_argument('-c','--calibration_file',type=str,required=True,help='path to calibration file (.npz)')
    args = vars(parser.parse_args())
    print(args)
    return args

def main():
    args = parse_input()

    calib_data  = np.load(args['calibration_file'])

    width = 3840
    height = 2160

    cameraMatrix1  = calib_data['cameraMatrix1']
    cameraMatrix2 = calib_data['cameraMatrix2']
    distCoeffs1 = calib_data['distCoeffs1']
    distCoeffs2 = calib_data['distCoeffs2']
    R = calib_data['R']
    T = calib_data['T']
    E = calib_data['E']
    F = calib_data['F']

    R1, R2, P1, P2, Q, roi1, roi2 = cv2.stereoRectify(cameraMatrix1=cameraMatrix1,
                                          cameraMatrix2=cameraMatrix2,
                                          distCoeffs1=distCoeffs1,
                                          distCoeffs2=distCoeffs2,
                                          imageSize=(width,height),
                                          R=R,
                                          T=T,
                                          flags=cv2.CALIB_ZERO_DISPARITY,
                                          alpha= 1
                                          )

    k_optim, roi = cv2.getOptimalNewCameraMatrix(cameraMatrix2, distCoeffs2, (width, height), 1, (width, height))
    x, y, w, h = roi

    images = glob(os.path.join(args['image_directory'], '*.' + args['file_extension']))
    output_directory = os.path.join(args['image_directory'], 'undistorted')

    if not os.path.exists(output_directory):
        os.mkdir(output_directory)

    for img in images:
        undist = cv2.undistort(cv2.imread(img), cameraMatrix2, distCoeffs2, None, k_optim)
        undist = undist[y:y + h, x:x + w]
        img = os.path.basename(img)
        cv2.imwrite(os.path.join(output_directory, img), undist)

    print('New camera parameters:', w, h, k_optim[0, 0], k_optim[1, 1], k_optim[0, 2], k_optim[1, 2], *distCoeffs1)

if __name__=='__main__':
    main()
