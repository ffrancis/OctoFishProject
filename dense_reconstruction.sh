#!/bin/bash

source activate tensorflow

## Please change /PATH/TO/PROJECT to your specific path
cd /PATH/TO/PROJECT

mkdir colmap_model
mkdir dense_model

## COLMAP will do a better job undistorting itself, but this can be used if needed:
#python ./undistort_images.py -d images/ -e jpg -c
#calibration_20181008-PM1-C1-calib-SYNCHED_20181008-PM1-C2-calib-SNYCHED.npz

colmap feature_extractor \
  --database_path ./database.db \
  --image_path ./images/undistorted \
  --ImageReader.single_camera 1 \
  --SiftExtraction.estimate_affine_shape 1 \
  --SiftExtraction.domain_size_pooling 1

colmap feature_extractor \
 --database_path ./database.db \
 --image_path ./images \
 --ImageReader.camera_model OPENCV \ ## use OPENCV_FISHEYE or FULL_OPENCV if not sufficient, however the output parameters change
 --ImageReader.single_camera_per_folder 1

colmap exhaustive_matcher \ ## only use for <1000 images. Otherwise refer to colmap.sequential_matcher
  --database_path ./database.db \
  --SiftMatching.guided_matching 1

colmap mapper \
  --database_path ./database.db \
  --image_path ./images/undistorted \
  --output_path ./colmap_model

colmap image_undistorter \
 --image_path ./images \
 --input_path ./sparse/0 \
 --output_path ./dense_model \
 --output_type COLMAP \
 --max_image_size 2000

colmap patch_match_stereo \
 --workspace_path ./dense \
 --workspace_format COLMAP \
 --PatchMatchStereo.geom_consistency true

colmap stereo_fusion \
 --workspace_path ./dense \
 --workspace_format COLMAP \
 --input_type geometric \
 --output_path ./dense/fused.ply

colmap poisson_mesher \
 --input_path ./dense/fused.ply \
 --output_path ./dense/meshed-poisson.ply

## Only if prior step don't suffice:
# colmap image_undistorter \
#  --image_path ./images \
#  --input_path ./sparse/0 \
#  --output_path ./dense_model \
#  --output_type NVM \
#  --max_image_size 2000

# cd dense_model
# cp ../images/undistorted/* .

## OpenMVS is only used for high resolution texture reconstruction.
# /usr/local/bin/OpenMVS/InterfaceVisualSFM model.nvm
# /usr/local/bin/OpenMVS/DensifyPointCloud model.mvs
# /usr/local/bin/OpenMVS/ReconstructMesh model_dense.mvs
# /usr/local/bin/OpenMVS/RefineMesh --resolution-level 1 --use-cuda 0 model_dense_mesh.mvs
# /usr/local/bin/OpenMVS/TextureMesh --export-type obj -o model.obj model_dense_mesh_refine.mvs
