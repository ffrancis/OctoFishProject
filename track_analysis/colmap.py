#!/usr/bin/env python3

"""
Copyright 2018 Paul Nührenberg <paul.nuehrenberg@uni-konstanz.de>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
import struct
import pandas as pd
import os
from glob import glob
from copy import deepcopy
from multiprocessing import Pool
import quaternion
import cv2
import _pickle
from numpy.linalg import inv
from scipy.interpolate import splprep, splev
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d
from utils import load, tracks_to_pool, tracks_from_pool, write_ply

class Parser:

    def __init__(self, model_path, tracks_path, camera_ids, camera_names, input_format = 'png'):
        self.extrinsics_path = os.path.join(model_path, 'images.bin')
        self.intrinsics_path = os.path.join(model_path, 'cameras.bin')
        self.points3d_path = os.path.join(model_path, 'points3D.bin')
        self.tracks_path = tracks_path
        self.camera_ids = camera_ids
        self.camera_names = camera_names
        self.input_format = '.' + input_format
        self.extrinsics = []
        self.intrinsics = {}
        self.tracks = {}
        self.points3d = []
        self._extrinsics = False
        self._intrinsics = False
        self._tracks = False
        self._points3d = False

    def read_next_bytes(self, f, num_bytes, format_char_sequence, endian_character = '<'):
        data = f.read(num_bytes)
        return struct.unpack(endian_character + format_char_sequence, data)

    def parse_extrinsics(self):
        images = []
        with open(self.extrinsics_path, 'rb') as f:
            num_images = self.read_next_bytes(f, 8, 'Q')[0]
            for image_idx in np.arange(num_images):
                binary_image_properties = self.read_next_bytes(f, num_bytes = 64, format_char_sequence = 'idddddddi')
                image_name = ''
                current_char = self.read_next_bytes(f, 1, 'c')[0]
                while current_char != b'\x00':
                    image_name += current_char.decode('utf-8')
                    current_char = self.read_next_bytes(f, 1, 'c')[0]
                binary_image_properties = list(binary_image_properties)
                binary_image_properties.append(image_name)
                images.append(binary_image_properties)
                num_points2D = self.read_next_bytes(f, num_bytes = 8, format_char_sequence = 'Q')[0]
                x_y_id_s = self.read_next_bytes(f, num_bytes = 24 * num_points2D, format_char_sequence = 'ddq' * num_points2D)
        self.extrinsics = pd.DataFrame(images, columns = ['IMAGE_ID', 'QW', 'QX', 'QY', 'QZ', 'TX', 'TY', 'TZ', 'CAMERA_ID', 'IMAGE_NAME'])
        self.extrinsics.IMAGE_ID = self.extrinsics.IMAGE_ID.astype(np.int)
        self.extrinsics.QW = self.extrinsics.QW.astype(np.float)
        self.extrinsics.QX = self.extrinsics.QX.astype(np.float)
        self.extrinsics.QY = self.extrinsics.QY.astype(np.float)
        self.extrinsics.QZ = self.extrinsics.QZ.astype(np.float)
        self.extrinsics.TX = self.extrinsics.TX.astype(np.float)
        self.extrinsics.TY = self.extrinsics.TY.astype(np.float)
        self.extrinsics.TZ = self.extrinsics.TZ.astype(np.float)
        self.extrinsics.CAMERA_ID = self.extrinsics.CAMERA_ID.astype(np.int) # np.repeat(1, len(self.extrinsics.IMAGE_ID)) # check this out
        self.extrinsics.IMAGE_NAME = self.extrinsics.IMAGE_NAME.astype(np.str)
        self.extrinsics = self.extrinsics.sort_values('IMAGE_ID')
        self._extrinsics = True

    def return_extrinsics(self, camera_id):
        if not self._extrinsics:
            self.parse_extrinsics()
        extrinsics = self.extrinsics[self.extrinsics.CAMERA_ID == camera_id].to_dict('list')
        return extrinsics

    def parse_intrinsics(self):
        cameras = {}
        with open(self.intrinsics_path, 'rb') as f:
            num_cameras = self.read_next_bytes(f, 8, 'Q')[0]
            for camera_line_index in range(num_cameras):
                camera_properties = self.read_next_bytes(f, num_bytes = 24, format_char_sequence = 'iiQQ')
                camera_id = camera_properties[0]
                num_params = 8
                params = self.read_next_bytes(f, num_bytes = 8 * num_params, format_char_sequence = 'd' * num_params)
                cameras[camera_id] = np.array(params)
        self.intrinsics = cameras
        self._intrinsics = True

    def return_intrinsics(self, camera_id):
        if not self._intrinsics:
            self.parse_intrinsics()
        intrinsics = self.intrinsics[camera_id]
        return intrinsics

    def parse_tracks(self):
        for camera_id, camera_name in zip(self.camera_ids, self.camera_names):
            with open(os.path.join(self.tracks_path, camera_name + '.pkl'), 'rb') as f:
                self.tracks[str(camera_id)] = _pickle.load(f)
        self._tracks = True

    def return_tracks(self, camera_id):
        if not self._tracks:
            self.parse_tracks()
        tracks = self.tracks[str(camera_id)]
        return tracks

    def parse_points3d(self):
        points3d = []
        with open(self.points3d_path, 'rb') as f:
            num_points = self.read_next_bytes(f, 8, 'Q')[0]
            for point_line_index in range(num_points):
                binary_point_line_properties = self.read_next_bytes(f, num_bytes = 43, format_char_sequence = 'QdddBBBd')
                point3D_id = binary_point_line_properties[0]
                xyz = np.array(binary_point_line_properties[1:4])
                rgb = np.array(binary_point_line_properties[4:7])
                error = np.array(binary_point_line_properties[7])
                track_length = self.read_next_bytes(f, num_bytes = 8, format_char_sequence = 'Q')[0]
                track_elems = self.read_next_bytes(f, num_bytes = 8 * track_length, format_char_sequence = 'ii' * track_length)
                points3d.append(np.concatenate([xyz,rgb]))
        self.points3d = np.array(points3d).reshape(-1, 6)
        self._points3d = True

    def return_points3d(self):
        if not self._points3d:
            self.parse_points3d()
        return self.points3d

class Camera:

    def __init__(self, camera_id, camera_name, parser, image_path, output_path, input_format = 'png', width = 3840, height = 2160):
        self.camera_id = camera_id
        self.camera_name = camera_name
        self.parser = parser
        self.image_path = image_path
        self.output_path = output_path
        self.input_format = '.' + input_format
        self.width = width
        self.height = height
        self.extrinsics = pd.DataFrame(columns = ['IMAGE_ID', 'QW', 'QX', 'QY', 'QZ', 'TX', 'TY', 'TZ', 'CAMERA_ID', 'IMAGE_NAME', 'FRAME_IDX'])
        self.rotations = np.array([]).reshape(0, 3, 3)
        self.translations = np.array([]).reshape(0, 3)
        self.k = np.array([])
        self.d = np.array([])
        self.frame_idx = np.array([])
        self.tracks = {}
        self._extrinsics = False
        self._intrinsics = False
        self._interpolated = False
        self._transformations = False
        self._tracks = False

    def get_extrinsics(self):
        extrinsics = self.parser.return_extrinsics(self.camera_id)
        image_names = np.array(extrinsics['IMAGE_NAME']).astype(np.str)
        image_names = np.array([os.path.basename((image)) for image in image_names])
        image_names = np.core.defchararray.replace(image_names, self.camera_name, '')
        image_names = np.core.defchararray.replace(image_names, '_', '')
        image_names = np.core.defchararray.replace(image_names, self.parser.input_format, '')
        mask = np.array([False if '/' in image else True for image in image_names])
        ## multiply by factor to correct for extracted frame rate:
        frame_idx = ((image_names[mask]).astype(np.int) * (25/3) - (25/3)).astype(np.int)
        for key in extrinsics:
            extrinsics[key] = np.array(extrinsics[key])[mask]

        extrinsics['FRAME_IDX'] = frame_idx

        self.extrinsics = pd.DataFrame(extrinsics)
        self._extrinsics = True

    def get_intrinsics(self):
        intrinsics = self.parser.return_intrinsics(self.camera_id)
        self.k = np.array([[intrinsics[0], 0, intrinsics[2]],
                           [0, intrinsics[1], intrinsics[3]],
                           [0, 0, 1]])
        self.d = intrinsics[4:8]
        self._intrinsics = True

    def interpolate(self, smooth_window = 0):
        if not self._extrinsics:
            self.get_extrinsics()
        self.extrinsics = self.extrinsics.sort_values('FRAME_IDX')
        extrinsics = [self.extrinsics.QW,
                      self.extrinsics.QX,
                      self.extrinsics.QY,
                      self.extrinsics.QZ,
                      self.extrinsics.TX,
                      self.extrinsics.TY,
                      self.extrinsics.TZ]
        frame_idx = np.arange(self.extrinsics.FRAME_IDX.min(), self.extrinsics.FRAME_IDX.max() + 1)
        for idx, parameter in enumerate(extrinsics):
            if smooth_window > 0:
                parameter = savgol_filter(parameter, smooth_window, 1)
            tck, u = splprep([self.extrinsics.FRAME_IDX, parameter], s = 0, k = 1)
            u = np.linspace(0, 1, frame_idx.size)
            parameter = splev(u, tck)[1]
            extrinsics[idx] = parameter
        self.extrinsics = pd.DataFrame({'IMAGE_ID': np.repeat(-1, frame_idx.size).astype(np.int),
                                        'QW': extrinsics[0],
                                        'QX': extrinsics[1],
                                        'QY': extrinsics[2],
                                        'QZ': extrinsics[3],
                                        'TX': extrinsics[4],
                                        'TY': extrinsics[5],
                                        'TZ': extrinsics[6],
                                        'CAMERA_ID': np.repeat(self.camera_id, frame_idx.size),
                                        'IMAGE_NAME': np.repeat('interpolated', frame_idx.size),
                                        'FRAME_IDX': frame_idx})
        self._interpolated = True

    def get_transformations(self):
        if not self._interpolated:
            self.interpolate()
        if not self._intrinsics:
            self.get_intrinsics()
        quaternions = quaternion.as_quat_array(np.transpose([self.extrinsics.QW, self.extrinsics.QX, self.extrinsics.QY, self.extrinsics.QZ]))
        self.rotations = quaternion.as_rotation_matrix(quaternions)
        self.translations = np.transpose([self.extrinsics.TX, self.extrinsics.TY, self.extrinsics.TZ])
        self.frame_idx = np.array(self.extrinsics.FRAME_IDX).astype(np.int)
        self._transformations = True

    def get_tracks(self):
        self.tracks = self.parser.return_tracks(self.camera_id)
        self._tracks = True

    def transform_tracks(self):
        if not self._tracks:
            self.get_tracks()
        if not self._transformations:
            self.get_transformations()
        tracks = tracks_to_pool(self.tracks)
        reconstructed_frames = (tracks['FRAME_IDX'] >= self.frame_idx[0]) & (tracks['FRAME_IDX'] <= self.frame_idx[-1])
        tracks['X'] = tracks['X'][reconstructed_frames]
        tracks['Y'] = tracks['Y'][reconstructed_frames]
        tracks['IDENTITY'] = tracks['IDENTITY'][reconstructed_frames]
        tracks['FRAME_IDX'] = tracks['FRAME_IDX'][reconstructed_frames]
        xy = np.transpose([tracks['X'], tracks['Y']]).reshape(-1, 1, 2).astype(np.float)
        xy = cv2.undistortPoints(xy, self.k, self.d).reshape(-1, 2) # fisheye
        tracks['X'] = xy[:, 0]
        tracks['Y'] = xy[:, 1]
        tracks['Z'] = np.repeat(np.nan, xy.shape[0])
        for idx, f_idx in enumerate(self.frame_idx):
            xy = np.transpose([tracks['X'], tracks['Y'], np.repeat(1, tracks['X'].size)])[tracks['FRAME_IDX'] == f_idx]
            if xy.shape[0] > 0:
                transformed_xy = np.array([]).reshape(0, 3)
                for point in xy:
                    point = point.reshape(1, 3) - self.translations[self.frame_idx == f_idx].reshape(1, 3)
                    point = point @ inv(self.rotations[self.frame_idx == f_idx].reshape(3, 3)).T
                    transformed_xy = np.append(transformed_xy, point.reshape(1, 3), axis = 0)
                transformed_xy = transformed_xy.reshape(-1, 3)
                tracks['X'][tracks['FRAME_IDX'] == f_idx] = transformed_xy[:, 0]
                tracks['Y'][tracks['FRAME_IDX'] == f_idx] = transformed_xy[:, 1]
                tracks['Z'][tracks['FRAME_IDX'] == f_idx] = transformed_xy[:, 2]
        self.tracks = tracks_from_pool(tracks)

class Scene:

    def __init__(self, model_path, tracks_path, camera_ids, camera_names, image_path = '', output_path = '', input_format = 'png'):
        self.model_path = model_path
        self.image_path = image_path
        self.output_path = output_path
        self.tracks_path = tracks_path
        self.camera_ids = camera_ids
        self.camera_names = camera_names
        self.parser = Parser(self.model_path, self.tracks_path, self.camera_ids, self.camera_names, input_format=input_format)
        self.cameras = {}
        self._cameras = False
        for camera_id, camera_name in zip(self.camera_ids, self.camera_names):
            self.cameras[camera_id] = Camera(camera_id, camera_name, self.parser, self.image_path, self.output_path)
        self.points3d = []
        self.tracks_3d = {}

    def reconstruct_cameras(self):
        for i in self.cameras:
            self.cameras[i].get_transformations()
            if not self.cameras[i]._transformations:
                return
        self._cameras = True

    def get_points3d(self):
        self.points3d = self.parser.return_points3d()

    def generate_pointcloud(self,output):
        r = 255
        g = 0
        b = 144
        if not os.path.exists(output):
            os.mkdir(output)
        for i in self.tracks_3d['IDENTITIES']:
            ply_tracks = np.vstack([self.tracks_3d[str(i)]['X'],self.tracks_3d[str(i)]['Y'],self.tracks_3d[str(i)]['Z'],np.repeat(r,self.tracks_3d[str(i)]['X'].size),np.repeat(g,self.tracks_3d[str(i)]['X'].size),np.repeat(b,self.tracks_3d[str(i)]['X'].size)]).T
            points = []
            for point in ply_tracks:
                points.append("%f %f %f %d %d %d 0\n"%(point[0],point[1],point[2],point[3],point[4],point[5]))
            write_ply(points,os.path.join(output,str(i)+'_tracks.ply'))
        points = []
        for point in self.points3d:
            points.append("%f %f %f %d %d %d 0\n"%(point[0],point[1],point[2],point[3],point[4],point[5]))
        write_ply(points,os.path.join(output,'environment.ply'))

    # def triangulate_multi_tracks(self):

    def triangulate_paired_tracks(self, cam_id_1, cam_id_2, i_1, i_2):
        shared_frame_idx = np.unique(np.concatenate([self.cameras[cam_id_1].tracks[str(i_1)]['FRAME_IDX'], self.cameras[cam_id_2].tracks[str(i_2)]['FRAME_IDX']])).astype(np.int)
        x1 = []
        x2 = []
        y1 = []
        y2 = []
        valid_frame_idx = []
        for frame_idx in shared_frame_idx:
            if (frame_idx in self.cameras[cam_id_1].tracks[str(i_1)]['FRAME_IDX']) and (frame_idx in self.cameras[cam_id_2].tracks[str(i_2)]['FRAME_IDX']) and (frame_idx in self.cameras[cam_id_1].frame_idx) and (frame_idx in self.cameras[cam_id_2].frame_idx):
                x1.append(self.cameras[cam_id_1].tracks[str(i_1)]['X'][self.cameras[cam_id_1].tracks[str(i_1)]['FRAME_IDX'] == frame_idx][0])
                x2.append(self.cameras[cam_id_2].tracks[str(i_2)]['X'][self.cameras[cam_id_2].tracks[str(i_2)]['FRAME_IDX'] == frame_idx][0])
                y1.append(self.cameras[cam_id_1].tracks[str(i_1)]['Y'][self.cameras[cam_id_1].tracks[str(i_1)]['FRAME_IDX'] == frame_idx][0])
                y2.append(self.cameras[cam_id_2].tracks[str(i_2)]['Y'][self.cameras[cam_id_2].tracks[str(i_2)]['FRAME_IDX'] == frame_idx][0])
                valid_frame_idx.append(frame_idx)
        if len(valid_frame_idx) > 0:
            x1 = np.array(x1)
            x2 = np.array(x2)
            y1 = np.array(y1)
            y2 = np.array(y2)
            valid_frame_idx = np.array(valid_frame_idx)
            xy1 = np.transpose([x1, y1]).reshape(-1, 1, 2).astype(np.float)
            xy1 = cv2.undistortPoints(xy1, self.cameras[cam_id_1].k, self.cameras[cam_id_1].d).reshape(-1, 2) ## remove cv2.fisheye.undistortPoints()
            x1 = xy1[:, 0]
            y1 = xy1[:, 1]
            xy2 = np.transpose([x2, y2]).reshape(-1, 1, 2).astype(np.float)
            xy2 = cv2.undistortPoints(xy2, self.cameras[cam_id_2].k, self.cameras[cam_id_2].d).reshape(-1, 2) ## remove cv2.fisheye.undistortPoints()
            x2 = xy2[:, 0]
            y2 = xy2[:, 1]
            points3d = []
            for frame_idx in valid_frame_idx:
                xy1 = np.array([x1[valid_frame_idx == frame_idx][0], y1[valid_frame_idx == frame_idx][0]]).reshape(2, 1)
                xy2 = np.array([x2[valid_frame_idx == frame_idx][0], y2[valid_frame_idx == frame_idx][0]]).reshape(2, 1)
                r1 = (self.cameras[cam_id_1].rotations[self.cameras[cam_id_1].frame_idx == frame_idx]).reshape(3, 3)
                r2 = (self.cameras[cam_id_2].rotations[self.cameras[cam_id_2].frame_idx == frame_idx]).reshape(3, 3)
                t1 = (self.cameras[cam_id_1].translations[self.cameras[cam_id_1].frame_idx == frame_idx]).reshape(3, 1)
                t2 = (self.cameras[cam_id_2].translations[self.cameras[cam_id_2].frame_idx == frame_idx]).reshape(3, 1)
                p1 = np.append(r1, t1, axis = 1)
                p2 = np.append(r2, t2, axis = 1)
                p4d = cv2.triangulatePoints(p1, p2, xy1, xy2)
                p3d = cv2.convertPointsFromHomogeneous(p4d.reshape(1, 4))
                points3d.append(p3d.reshape(3))
            points3d = np.array(points3d)
        else:
            points3d = np.array([]).reshape(0, 3)
            valid_frame_idx = np.array([])
        return points3d, valid_frame_idx

    def triangulate_tracks_3d(self): # def triangulate_tracks_3d(self, correspondences_file):
        if not self._cameras:
            self.reconstruct_cameras()
        # correspondences = load(correspondences_file)
        tracks_3d = {'IDENTITIES': [], 'FRAME_IDX': []}
        for identity in self.cameras[self.camera_ids[0]].tracks['IDENTITIES']:
            tracks_3d[str(identity)] = {'X': [], 'Y': [], 'Z': [], 'FRAME_IDX': []}
            tracks_3d['IDENTITIES'] = np.append(tracks_3d['IDENTITIES'], identity).astype(np.uint)
            # for i in correspondences[identity]['I']:
            #     for c in correspondences[identity]['C']:
            points3d, valid_frame_idx = self.triangulate_paired_tracks(self.camera_ids[0], self.camera_ids[1], identity, identity)
            tracks_3d[str(identity)]['X'] = np.append(tracks_3d[str(identity)]['X'], points3d[:, 0])
            tracks_3d[str(identity)]['Y'] = np.append(tracks_3d[str(identity)]['Y'], points3d[:, 1])
            tracks_3d[str(identity)]['Z'] = np.append(tracks_3d[str(identity)]['Z'], points3d[:, 2])
            tracks_3d[str(identity)]['FRAME_IDX'] = np.append(tracks_3d[str(identity)]['FRAME_IDX'], valid_frame_idx)
            tracks_3d['FRAME_IDX'] = np.append(tracks_3d['FRAME_IDX'], valid_frame_idx.astype(np.uint))
            sorted_idx = np.argsort(tracks_3d[str(identity)]['FRAME_IDX'])
            tracks_3d[str(identity)]['X'] = tracks_3d[str(identity)]['X'][sorted_idx]
            tracks_3d[str(identity)]['Y'] = tracks_3d[str(identity)]['Y'][sorted_idx]
            tracks_3d[str(identity)]['Z'] = tracks_3d[str(identity)]['Z'][sorted_idx]
            tracks_3d[str(identity)]['FRAME_IDX'] = tracks_3d[str(identity)]['FRAME_IDX'][sorted_idx]
        tracks_3d['FRAME_IDX'] = np.unique(tracks_3d['FRAME_IDX']).astype(np.uint)
        self.tracks_3d = tracks_3d
    #
    # def estimate_tracks_3d(self):
    #     self.get_points3d()
    #     mean_z = np.mean(self.points3d[:,2])
    #     x_range = np.max(self.points3d[:,0]) - np.min(self.points3d[:,0])
    #     y_range = np.max(self.points3d[:,1]) - np.min(self.points3d[:,1])
    #     cam_id = self.camera_ids[0]
    #     positions = []
    #     for frame_idx in self.cameras[cam_id].frame_idx:
    #         idx = np.argwhere(self.cameras[cam_id].frame_idx == frame_idx)[0][0]
    #         positions.append(-self.cameras[cam_id].rotations[idx].reshape(3, 3).T @ self.cameras[cam_id].translations[idx].reshape(3))
    #     positions = np.array(positions).reshape(-1, 3)
    #     tracks_2d = self.cameras[cam_id].tracks
    #     for i in tracks_2d['IDENTITIES']:
    #         # z = np.repeat(mean_z, tracks_2d[str(i)]['X'].size)
    #         for f in tracks_2d[str(i)]['FRAME_IDX']:
    #             if f in self.cameras[cam_id].frame_idx:
    #                 pos_cam = positions[self.cameras[cam_id].frame_idx == f].reshape(3, 1)
    #                 pos_2d = np.array([tracks_2d[str(i)]['X'][tracks_2d[str(i)]['FRAME_IDX'] == f][0],
    #                                    tracks_2d[str(i)]['Y'][tracks_2d[str(i)]['FRAME_IDX'] == f][0],
    #                                    tracks_2d[str(i)]['Z'][tracks_2d[str(i)]['FRAME_IDX'] == f][0]]).reshape(3, 1)
    #                 ray = pos_2d - pos_cam
    #                 z_3d = np.mean(self.points3d[:, 2][(np.absolute(self.points3d[:, 0] - pos_2d[0]) < x_range / 20) & (np.absolute(self.points3d[:, 1] - pos_2d[1]) < y_range / 20)])
    #                 # z_3d = z[tracks_2d[str(i)]['FRAME_IDX'] == f][0]
    #                 scale = (z_3d - pos_cam[2][0]) / ray[2][0]
    #                 pos_3d = pos_cam + ray * scale
    #                 tracks_2d[str(i)]['X'][tracks_2d[str(i)]['FRAME_IDX'] == f] = pos_3d[0][0]
    #                 tracks_2d[str(i)]['Y'][tracks_2d[str(i)]['FRAME_IDX'] == f] = pos_3d[1][0]
    #                 tracks_2d[str(i)]['Z'][tracks_2d[str(i)]['FRAME_IDX'] == f] = pos_3d[2][0]
    #     self.cameras[cam_id].tracks = tracks_2d
