#!/usr/bin/env python3

"""
Copyright 2018 Paul Nührenberg <paul.nuehrenberg@uni-konstanz.de>
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
import struct
import pandas as pd
import cv2
import _pickle
import os
import matplotlib.pyplot as plt

from glob import glob
from copy import deepcopy
from multiprocessing import Pool
from numpy.linalg import inv
from scipy.interpolate import splprep, splev
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d
from mpl_toolkits.mplot3d import axes3d

def save(dump, file_name):
    with open(file_name, 'wb') as fid:
        _pickle.dump(dump, fid)
    return True

def load(file_name):
    with open(file_name, 'rb') as fid:
        dump = _pickle.load(fid)
    return dump

def write_tracks_pooled(tracks, file_name):
    if 'IDENTITIES' in tracks:
        tracks = tracks_to_pool(tracks)
    else:
        tracks = get_direction(tracks)
    track_data = pd.DataFrame.from_dict(tracks)
    track_data.to_csv(file_name)
    return True

def tracks_to_pool(tracks):
    x = np.array([])
    y = np.array([])
    z = np.array([])
    frame_idx = np.array([], dtype = np.int)
    identity = np.array([], dtype = np.int)
    direction = np.array([])
    for i in tracks['IDENTITIES']:
        x = np.append(x, tracks[str(i)]['X'])
        y = np.append(y, tracks[str(i)]['Y'])
        frame_idx = np.append(frame_idx, tracks[str(i)]['FRAME_IDX'])
        identity = np.append(identity, np.repeat(i, tracks[str(i)]['FRAME_IDX'].size))
        if 'Z' in tracks[str(i)]:
            z = np.append(z, tracks[str(i)]['Z'])
        if 'DIRECTION' in tracks[str(i)]:
            direction = np.append(direction, tracks[str(i)]['DIRECTION'])
    tracks = {}
    tracks['X'] = x
    tracks['Y'] = y
    if z.size > 0:
        tracks['Z'] = z
    tracks['FRAME_IDX'] = frame_idx
    tracks['IDENTITY'] = identity
    if direction.size > 0:
        tracks['DIRECTION'] = direction
    return tracks

def tracks_from_pool(tracks, min_size = 5):
    x = tracks['X']
    y = tracks['Y']
    frame_idx = tracks['FRAME_IDX']
    identity = tracks['IDENTITY']
    tracks = {}
    tracks['FRAME_IDX'] = np.array([]).astype(np.int)
    tracks['IDENTITIES'] = np.array([]).astype(np.int)
    for i in np.unique(identity):
        if np.sum(identity == i) > min_size:
            tracks[str(i)] = {}
            tracks[str(i)]['X'] = x[identity == i]
            tracks[str(i)]['Y'] = y[identity == i]
            tracks[str(i)]['FRAME_IDX'] = frame_idx[identity == i]
            tracks['FRAME_IDX'] = np.append(tracks['FRAME_IDX'], tracks[str(i)]['FRAME_IDX'])
            tracks['IDENTITIES'] = np.append(tracks['IDENTITIES'], i)
    tracks['FRAME_IDX'] = np.unique(tracks['FRAME_IDX'])
    return tracks

def interpolate_tracks(tracks, smooth_window = 11, degree = 1, masked = False):
    tracks = remove_duplicates(tracks)
    tracks['FRAME_IDX'] = np.array([])
    for i in tracks['IDENTITIES']:
        frame_idx = np.arange(tracks[str(i)]['FRAME_IDX'][0], tracks[str(i)]['FRAME_IDX'][-1] + 1)
        if masked:
            mask = deepcopy(tracks[str(i)]['FRAME_IDX'])
        interp_x = interp1d(tracks[str(i)]['FRAME_IDX'], tracks[str(i)]['X'])
        interp_y = interp1d(tracks[str(i)]['FRAME_IDX'], tracks[str(i)]['Y'])
        tracks[str(i)]['X'] = savgol_filter(interp_x(frame_idx), smooth_window, degree)
        tracks[str(i)]['Y'] = savgol_filter(interp_y(frame_idx), smooth_window, degree)
        if 'Z' in tracks[str(i)]:
            interp_z = interp1d(tracks[str(i)]['FRAME_IDX'], tracks[str(i)]['Z'])
            tracks[str(i)]['Z'] = savgol_filter(interp_z(frame_idx), smooth_window, degree)
        tracks[str(i)]['FRAME_IDX'] = frame_idx
        if masked:
            for key in tracks[str(i)]:
                tracks[str(i)][key] = tracks[str(i)][key][np.isin(frame_idx, mask)]
        tracks['FRAME_IDX'] = np.append(tracks['FRAME_IDX'], tracks[str(i)]['FRAME_IDX'])
    tracks['FRAME_IDX'] = np.unique(tracks['FRAME_IDX']).astype(np.int)
    return tracks

def write_ply(points,output):
    file = open(output,"w")
    file.write('''ply
    format ascii 1.0
    element vertex %d
    property float x
    property float y
    property float z
    property uchar red
    property uchar green
    property uchar blue
    property uchar alpha
    end_header
    %s
    '''%(len(points),"".join(points)))
    file.close()

def remove_duplicates(tracks):
    tracks['FRAME_IDX'] = np.array([])
    for i in tracks['IDENTITIES']:
        valid = np.where(tracks[str(i)]['FRAME_IDX'] != np.roll(tracks[str(i)]['FRAME_IDX'], -1))
        tracks[str(i)]['X'] = tracks[str(i)]['X'][valid]
        tracks[str(i)]['Y'] = tracks[str(i)]['Y'][valid]
        tracks[str(i)]['FRAME_IDX'] = tracks[str(i)]['FRAME_IDX'][valid]
        tracks['FRAME_IDX'] = np.append(tracks['FRAME_IDX'], tracks[str(i)]['FRAME_IDX'])
    tracks['FRAME_IDX'] = np.unique(tracks['FRAME_IDX'])
    return tracks

def plot_tracks(tracks, label = False, group = False, identity = False, colormap = [], style = 'k-', return_figure = False):
    fig, ax = plt.subplots(figsize = (50, 50))
    for i in tracks['IDENTITIES']:
        if label or group and hasattr(colormap, 'colors'):
            l = 'LABEL'
            if group:
                l = 'GROUP'
            ax.scatter(tracks[str(i)]['X'], tracks[str(i)]['Y'], label = str(i), c = np.array(colormap.colors)[tracks[str(i)][l]], s = 2, edgecolor = 'none')
        elif identity:
            ax.plot(tracks[str(i)]['X'], tracks[str(i)]['Y'], '-', label = str(i))
        else:
            ax.plot(tracks[str(i)]['X'], tracks[str(i)]['Y'], style, label = str(i))
    ax.set_aspect('equal')
    ax.set_xticks([])
    ax.set_yticks([])
    if not return_figure:
        plt.show()
    else:
        return ax

def plot_tracks_3d(tracks_3d):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in tracks_3d['IDENTITIES']:
        ax.plot(-tracks_3d[str(i)]['X'], tracks_3d[str(i)]['Y'], tracks_3d[str(i)]['Z'], '.-', markersize = 0.5, linewidth = 0.5)
    X = np.concatenate([-tracks_3d[str(i)]['X'] for i in tracks_3d['IDENTITIES']])
    Y = np.concatenate([tracks_3d[str(i)]['Y'] for i in tracks_3d['IDENTITIES']])
    Z = np.concatenate([tracks_3d[str(i)]['Z'] for i in tracks_3d['IDENTITIES']])
    ax.set_aspect('equal')
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0
    mid_x = (X.max()+X.min()) * 0.5
    mid_y = (Y.max()+Y.min()) * 0.5
    mid_z = (Z.max()+Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    plt.show()
