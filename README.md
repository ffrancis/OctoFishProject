# Stereo calibration:

This script is designed to calibrate a stereo camera and save the
required calibration metrics in an output file (.npz). Further verbosity of
the viewable output can be enabled by setting the equivalent flag (see
below).

The script is designed to pick the defined number of random calibration images from the input files to ensure more a more rigorous calibration.

## Usage

Reliably tested with these versions:

|Dependencies|Version |
|:---|:---:|
|Python|3.7.0 |
|OpenCV|3.4.3|
|numpy|1.15.2|
|matplotlib|2.2.3|

In order to use the stereo calibration script (command line interface)
navigate to the
containing folder and execute
```
python stereo_calib.py
```
followed by the additional arguments for specification:

|Argument       | Use           |Default |
|:-------------: |:-------------| :-----|
|-r|path to right input video|no default|
|-l|path to left input video|no default|
|-v|define verbosity, False/True|False|
|-n|number of calibration images that should be acquired|20|
|-c|checkerboard pattern (rows,columns)|9 7|
|-o|output path|current dir|

<p align="center">
<img
src="images/checkerboard_positions.png"
width="800">
</p>

Image 1.: Sample verbose output showing estimated 3D positions of
checkerboard corners of five calibration frames.

### Example

```
python stereo_calib.py -l 20181008-PM1-C1-calib-SYNCHED.mp4 -r 20181008-PM1-C2-calib-SNYCHED.mp4 -n 5 -c 8 6
```

# View calibration results:

The calibration output (.npz) can also be viewed separately by using the ```plot_calibration_results.py``` script.

### Example

```
python plot_calibration_results.py -i *.npz
```

# Undistortion:

Images can be undistorted using the ```undistort_images.py``` script which takes the output file (.npz) from ```stereo_calib.py``` as input. Images need to be created from calibration videos (using ```ffmpeg```) in a subdirectory ```images```.
The undistorting is also incorporated into the 3d reconstruction pipeline (see below).

|Argument       | Use           |Default |
|:-------------: |:-------------| :-----|
|-d|path to image directory|no default|
|-e|file extension|no default|
|-c|path to calibration file (.npz)|no default|

### Example

```
python undistort_images.py -d images/ -e jpg -c *.npz
```

# 3D Reconstruction:

Reconstruction is done by using COLMAP (https://colmap.github.io/) and OpenMVS (http://cdcseacave.github.io/openMVS/) and depends on these being installed first. For this we recommend the following docker installation which incorporates much of the needed functionality: https://github.com/rennu/dpg.

The provided script incorporates the entire COLMAP/OpenMVS pipeline from image undistortion (images need to be located in a subdirectory ```images```) to a full, textured dense reconstruction (.nvm).

### Example

```
./dense_reconstruction.sh
```
<p align="center">
<img src="images/3d_animation.gif" width="600">
</p>

Image 2.: Animated sparse 3d reconstruction of a coral reef done using COLMAP.

# Track analysis:

All track analysis is done using the ```track_analysis/workon_tracks.ipynb``` jupyter notebook. For doing so we recommend creating a python environment with the supplied ```track_analysis/environment.yml``` file. This can be done by running the following command:

```
conda env create -f environment.yml
```
The created environment is named ```3dtracking```.  
An overview of all ```conda``` environments is given by running:

```
conda info --envs
```

If this helps, you have comments about the software or you have further
requests please feel free to email me [fritz.a.francisco@gmail.com].
